# Updated README for Sample code

# Short description
Here in this sample code we are showing folder structure that we use to build a app
Below are some brief explanation of folder structure.

# src/app/middleware
Here in this folder we add all the middleware processes which need to be working . for eg. adding token in every request, sowing loader on every request. etc.

# src/app/pages
It contain all the pages of web app like login, registration etc.
If any page come after login we can specify it under admin or we can make a new component in new folder inside pages.

# src/app/services
It contain all services classes that we use in our app

# src/app/shared_layouts
It contain shared html part that may be re-usable with different component like "datatable", sidebar etc.

# src/app/shared_modules
This folder contain all modules which may be reusable with different modules like "Formaly, datepicker or external modules"


# process of working with Formaly
It any component we need to use Formaly we create a ts file containing formaly json file which is imported in component and then tha imported file is use to make a formaly form.


# use of formly
We used formly to create forms that will be submitted.
Here in code sample we used form as
 login form
 Registration Form
 Task creation and updating form
For formly form we create a form.ts file which include JSON of formly.
For creating reusable and custom field we create them in shared module 
Here in code sample we have created a datepicker form .