import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoaderService } from '../services/loader.service';
import { ApiService } from '../services/api.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private loader: LoaderService, private apiService: ApiService) { }
  
// In this function we are using token in every request . This function is called before every request called
// and before every request return.
// We alson added loader service which will show a loader on every request start. And get automatically stop on request is returned.
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({ headers: request.headers.set('Content-type', 'application/json') })
    request = request.clone({
      headers: request.headers.set('Authorization', `Bearer ${this.apiService.getToken()}`)
    });
    this.loader.loader(true);
    return next.handle(request).pipe(
      map((event: HttpEvent<any>, error) => {
        this.loader.loader(false);
        return event;
      }));
  }
}