import { Component } from '@angular/core';
import {LoaderService} from './services/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'EBSlive';
  loaderFlag=false
  constructor(private loader:LoaderService){
    this.loader.loaderFlag().subscribe((data:boolean)=>{
        this.loaderFlag = data;
        // loderEvent.unsubscribe();
    })
  }
}
