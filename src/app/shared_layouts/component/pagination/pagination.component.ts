import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() config: any;
  @Output() changePage = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {
  
  }
  pagesList(){
    this.config.currentPage = (this.config.skip/this.config.perPage)+1;
    let totalPages = Math.ceil(this.config.totalRecord/this.config.perPage);
    let pageList = []
    for(let i=1; i<= totalPages;i++){
      pageList.push(i);
    }
    return pageList;
  }
  change_Page(page:number){
    return this.changePage.emit(page);
  }

  onNextPage(): void {
    console.log('this.config.currentPage',this.config.currentPage)
    if(this.config.currentPage == this.pagesList().length){
      return
    }
    this.changePage.emit(this.config.currentPage+1);
  }

  onPreviousPage(): void {
    if(this.config.currentPage == 1){
      return
    }
    this.changePage.emit(this.config.currentPage-1);
  }
 

}
