import { Component, EventEmitter, Input, OnInit, Output, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import * as env from 'src/environments/environment';
@Component({
  selector: 'app-theme-table',
  templateUrl: './theme-table.component.html',
  styleUrls: ['./theme-table.component.scss'],
})
export class ThemeTableComponent implements OnInit {
  @Input() dataTable: any;
  @Input() option: any;
  @Input() module: any;
  @Input() projectTitle: string;
  initially: boolean;
  filterDialog: boolean = false;
  deleteCheckboxDialog: boolean = false;
  showPopUp: number;
  dataPerpage: number;
  urlParameter: string;
  checkboxCount: number = 0;
  isChecked: boolean = false;
  paginationConfig = { totalCount: 10, perPage: 10, currentPage: 1 };
  searchText: string = '';
  interval: any;
  @Output() deleteData = new EventEmitter<any>();
  @Output() duplicateData = new EventEmitter<any>();
  @Output() anyChangeInConfig = new EventEmitter<any>();
  users = [
    {
      name: '',
    },
  ];
  appUrl: String;
  constructor(private apiService: ApiService, private router: Router) {
    this.appUrl = env.environment.appUrl;
  }

  ngOnInit(): void {
  }

  searchTextChange(){
    this.dataTable.skip=0;
    for(let field of this.dataTable.headers){
        field.order = 0;
      }
    this.anyChangeInConfig.emit(this.dataTable);
  }

  onPageChange(event: any) {
    // debugger
    this.dataTable.skip = (event-1)*this.dataTable.perPage;
    this.anyChangeInConfig.emit(this.dataTable);
  }

  // @HostListener('click')
  onOpenToggle(index: any) {
    if(this.showPopUp == index){
      this.showPopUp = .1
      if(this.interval){
        clearInterval(this.interval)
      }
    } else {
      this.showPopUp = index;
      this.interval = setTimeout(()=>this.showPopUp = 0.1, 1500)
    }
    
  }

  // @HostListener('document:click', ['$event'])
  // @HostListener('click')
  clickout() {
   this.showPopUp = 0.1
  }

  enterPopup(){
    if(this.interval){
      clearInterval(this.interval)
    }
  }
  // update(id: any) {
  //   this.router.navigate([this.appUrl+'admin/'+(this.module)+'/update/'+id])
  // }

  onDuplicate(id: any) {
    this.initially = false;
    return this.duplicateData.emit(id);
  }
  onDelete(id: any) {
    return this.deleteData.emit(id);
  }

  onCheckBoxchange(event: any, id: any) {
    if (event.target.checked) {
      this.deleteCheckboxDialog = true;
      this.checkboxCount++;
    } else {
      this.checkboxCount--;
    }
  }
  openFilterDialog() {
    this.filterDialog = !this.filterDialog;
  }
  getDataPerPage() {
    this.dataTable.skip=0;
    return this.anyChangeInConfig.emit(this.dataTable);
  }

  onGetAllCheckbox(event: any) {
    if (event.target.checked) {
      console.log(this.dataPerpage);
      this.deleteCheckboxDialog = true;
      this.isChecked = !this.isChecked;
      this.checkboxCount = this.dataTable.data.length || this.dataPerpage;
    } else {
      console.log(this.dataPerpage);
      this.deleteCheckboxDialog = false;
      this.isChecked = !this.isChecked;
      this.checkboxCount = 0;
    }


  }

  sortby(key: string){
    if(this.dataTable.headers){
    for(let field of this.dataTable.headers){
      if(key == field.key){
        if(field.order == 0){
          field.order = 1
        } else if (field.order == 1){
          field.order = -1
        } else if (field.order == -1){
          field.order = 0
        }
      } else {
        field.order = 0;
      }
    }
  }
  this.dataTable.skip = 0;
  this.anyChangeInConfig.emit(this.dataTable)
  
  }
}
