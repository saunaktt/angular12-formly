import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot,Router } from '@angular/router';
import {ApiService} from '../services/api.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router:Router,private apiService:ApiService){}
  // We use this function to check if user is loged in or not if not user 
  // is redirected to main page
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){
      if(this.apiService.loggedIn()){
        return true;
      }
      this.router.navigate([''])
    return false;
  }
  
}
