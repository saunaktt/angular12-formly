import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginRegistrationComponent } from './shared_layouts/login-registration/login-registration.component'
import { HeaderComponent } from './shared_layouts/header/header.component'
import { AuthGuard } from './guard/auth.guard';
import { PageNotFoundComponent } from './pages/not-found/page-not-found/page-not-found.component';
import {AdminComponent} from './pages/admin/admin.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: '/login',
    pathMatch: "full"
  },
  {
    path: "",
    component: HeaderComponent,
    children: [
      
      {
        path: "",
        component: LoginRegistrationComponent,
        children: [
          {
            path: "",
            loadChildren: () => import("./pages/login-registration/login-registration.module").then(m => m.LoginRegistrationModule)
          },
        ]
      },
      {
        path:'admin',
        component:AdminComponent,
        canActivate:[AuthGuard],
        children:[{
          path: "",
          loadChildren:()=>import('./pages/admin/admin.module').then(m=>m.AdminModule)
        }]
      }
      
    ],
  },
  {
    path:'**',
    component:PageNotFoundComponent,
  }
  



];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
