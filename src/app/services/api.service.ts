import { Injectable,EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { io } from "socket.io-client";
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient, private ToastrService: ToastrService) {
  }
  sharedVariable = {};// shared variable to get into different component

  /**
  * // TODO: Toaster messages 
  * Gets script version
  * @param flag 
  * @param message
  * @returns shows toaster
  */
  toaster(flag, message) {
    switch (flag) {
      case 'success': {
        this.ToastrService.success(message['code'], message['message']);
        break;
      }
      case 'warning': {
        this.ToastrService.warning(message['code'], message['message']);
        break;
      }
      case 'error': {
        this.ToastrService.error(message['code'], message['message']);
        break;
      }
    }
  }

  /**
  * // TODO: Insert data into share variable 
  * Gets script version
  * @param key 
  * @param value
  */
  makeshareVariable(key,value){
    this.sharedVariable[key] = value;
  }

  /**
  * // TODO: Common get method to for appending base url
  * Gets script version
  * @param url 
  * @returns data from API
  */
  get(url) {
    const endpoint = environment.liveUrl;
    return this.http
      .get(endpoint + url, {})
      .pipe(retry(1), catchError(this.handleError));
  }
  /**
  * // TODO: Decode user id from token
  * Gets script version
  * @returns returns User ID from token
  */
  getUserId(){
    if (localStorage.getItem('token') !== null) {
      var token = localStorage.getItem('token');
      var decoded:{_id,email} = jwt_decode(token?token:'');
      return decoded._id
    }
  }
   /**
  * // TODO: Checks whether user is logged in or not
  * Gets script version
  * @returns returns true if logged in or false if not logged in user
  */
  loggedIn(): boolean {
    console.log('login check!!')
    if (localStorage.getItem('token') !== null) {
      var token = localStorage.getItem('token');
      var decoded:{_id,email} = jwt_decode(token?token:'');
      
      return true
    } else {
      return false
    }
  }
   /**
  * // TODO: common method to get token from local storage
  * Gets script version
  * @returns token from local storage
  */
  getToken() {
    return localStorage.getItem('token');
  }

   /**
  * // TODO: Remove token from local storage 
  * Gets script version
  */
  logOut() {
    localStorage.removeItem('token');
  }

 /**
  * // TODO: Common post method which appends base url
  * Gets script version
  * @param url takes end point
  * @param data Takes request body
  * @returns API data
  */
  post(url, data) {
    const endpoint = environment.authUrl;

    return this.http
      .post(endpoint + url, data, {
        // headers: header
      })
      .pipe(retry(1), catchError(this.handleError));
  }

 
  
/**
  * // TODO: Common error handler
  * Gets script version
  * @param error takes error from response body
  * @returns error message
  */
  handleError = (error) => {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `${error.error.message}`;
    }
    // console.log(errorMessage);
    // this.ToastrService.error('',errorMessage);
    
      this.ToastrService.error(errorMessage, error['status']);
    
    
    return throwError(errorMessage);
  };
}
