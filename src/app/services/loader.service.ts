import { Injectable,EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  displayLoader: EventEmitter<number> = new EventEmitter();
  constructor() { }
  loader(flag){
    this.displayLoader.emit(flag)
    
  }
  loaderFlag(){
    return this.displayLoader;
  }
}
