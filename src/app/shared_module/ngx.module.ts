import { ModuleWithProviders, NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { BsModalService, ModalModule } from 'ngx-bootstrap/modal';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AccordionModule } from 'ngx-bootstrap/accordion';
// import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CarouselModule } from 'ngx-bootstrap/carousel';
// import { NgxStripeModule } from 'ngx-stripe';
// import { NgxPaginationModule } from 'ngx-pagination';
// import { PrettyJsonModule } from 'angular2-prettyjson';
// import { ToastContainerModule, ToastrModule } from 'ngx-toastr';
// import { EthosToastComponent } from '../components/ngx-toast/custom.toast';
// import { GlobalConfig } from 'ngx-toastr/toastr/toastr-config';
import { SortableModule } from 'ngx-bootstrap/sortable';
import { PopoverModule } from 'ngx-bootstrap/popover';




@NgModule({
  imports: [
    // CommonModule,
    // ToastContainerModule,
    // NgxDatatableModule,
    // NgxPaginationModule,
    // NgxStripeModule,
    SortableModule.forRoot(),
    ModalModule.forRoot(),
    CollapseModule.forRoot(),
    AccordionModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    CarouselModule.forRoot(),
    PopoverModule.forRoot(),
    // ToastrModule.forRoot(TOAST_CONFIG),
    // PrettyJsonModule,
  ],
  exports: [
    // ToastContainerModule,
    // NgxDatatableModule,
    // NgxPaginationModule,
    // NgxStripeModule,
    ModalModule,
    CollapseModule,
    AccordionModule,
    TooltipModule,
    TypeaheadModule,
    BsDatepickerModule,
    TabsModule,
    BsDropdownModule,
    CarouselModule,
    // ToastrModule,
    // PrettyJsonModule,
    SortableModule,
    PopoverModule
  ]
})

export class NgxModule {
  static forRoot(): ModuleWithProviders<NgxModule> {
    return {
      ngModule: NgxModule,
      providers: [
        BsModalService,
      ]
    };
  }

}

