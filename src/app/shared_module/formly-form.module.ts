import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule,AbstractControl } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyBootstrapModule } from '@ngx-formly/bootstrap';
import { CustomDatepickerComponent } from '../shared_layouts/component/custom-datepicker/custom-datepicker.component';
export function fieldMatchValidator(control: AbstractControl) {
  const { password, passwordConfirm } = control.value;
  // avoid displaying the message error when values are empty
  if (!passwordConfirm || !password) {
    return null;
  }

  if (passwordConfirm === password) {
    return null;
  }

  return { fieldMatch: { message: 'The password and confirmation password do not match' } };
}



@NgModule({
  declarations: [],
  imports: [
    ReactiveFormsModule,
    FormlyModule.forRoot({
      types: [
        { name: 'datePickr',
         component: CustomDatepickerComponent 
        }
      ],
      validators: [
        { name: 'fieldMatch', validation: fieldMatchValidator },
      ],
      validationMessages: [
        { name: 'required', message: 'This field is required' },
      ],
    }),
    FormlyBootstrapModule,
    CommonModule
  ],
  exports: [
    ReactiveFormsModule,
    FormlyModule,
    FormlyBootstrapModule
  ]
})
export class FormlyFormModule { }
