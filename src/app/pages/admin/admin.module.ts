import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { NgxModule } from '../../shared_module/ngx.module';
import { TasksComponent } from './tasks/tasks.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AdminComponent } from './admin.component';

const routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'tasks',
        component: TasksComponent,
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./tasks/tasks.module').then((m) => m.TaskModule),
          },
        ],
      },
    ],
  },
];

@NgModule({
  declarations: [
    TasksComponent,
    AdminComponent,
  ],
  imports: [
    CommonModule,
    TabsModule,
    NgxModule,
    RouterModule.forChild(routes),
  ],
})
export class AdminModule {}
