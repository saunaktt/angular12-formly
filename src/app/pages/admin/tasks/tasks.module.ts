import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NewTaskComponent } from './new-task/new-task.component';
import { ListingComponent } from './listing/listing.component';
import { FormlyFormModule } from '../../../shared_module/formly-form.module';
import { AdminModuleModule } from '../../../shared_module/admin-module.module';



const routes = [
  {
    path: '',
    redirectTo: 'listing',
    pathMatch: 'full'
  },
  {
    path: 'listing',
    component: ListingComponent,
  },
  {
    path: 'new_task',
    component: NewTaskComponent,
  },
  {
    path: 'update/:id',
    component: NewTaskComponent,
    data:{
      type:'update'
    }
  },
];

@NgModule({
  declarations: [
    NewTaskComponent,
    ListingComponent,
    // PaginationComponent,
    // PageTitleComponent,
    // ThemeTableComponent,
  ],
  imports: [    
    AdminModuleModule,
    FormsModule,
    FormlyFormModule,
    RouterModule.forChild(routes),
  ],
})
export class TaskModule {}
