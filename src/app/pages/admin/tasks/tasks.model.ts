export interface Tasks {
  taskName: string;
  project: string;
  customer: string;
  assignee: string;
  timeSpent: string;
  startDate: string;
  endDate: string;
  status: string;
}
