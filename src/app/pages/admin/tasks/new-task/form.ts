import { FormlyFieldConfig } from "@ngx-formly/core";
import * as moment from 'moment/moment.js';

export let formlyForm = (anyData:any)=>{
  return  [
    {
      key: 'taskName',
      type: 'input',
      templateOptions: {
        placeholder: 'Name',
        label: 'Task Name',
        required: true
      }
    },
    {
      type: 'textarea',
      key: 'taskDetails',
      templateOptions: {
        label: 'Task Description',
        placeholder: 'Description',
        rows: 6,
      },
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          key: 'project',
          type: 'select',
          templateOptions: {
            placeholder: 'Select Project',
            label: 'Project',
            required: true,
            options: anyData.project
          },
          required: true
        },
        {
          className: 'col-6',
          key: 'assignee',
          type: 'select',
          templateOptions: {
            placeholder: 'Select Assignee',
            label: 'Assignee',
            required: true,
            options:anyData.assignee
          },
        },
      ]
    },
    
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          key: 'startDate',
          type: 'datePickr',
          defaultValue: moment().toDate(),
          templateOptions: {
            placeholder: 'DD/MM/YY',
            label: 'Start date',
            dateFormat:'DD/MM/YY',
            required: true,
          },
        },
        {
          className: 'col-6',
          type: 'datePickr',
          key: 'endDate',
          required: true,
          defaultValue: moment().toDate(),
          templateOptions: {
            label: 'End date',
            placeholder: 'DD/MM/YY',
            dateFormat:'DD/MM/YY',
            required: true,
          },
  
        },
      ],
    },
    {
      fieldGroupClassName: 'row',
      fieldGroup: [
        {
          className: 'col-6',
          type: 'input',
          key: 'timeSpent',
          required: true,
          templateOptions: {
            type: 'text',
            placeholder: '00:00',
            required: true,
            label: 'Time spent',
          },
          validators: {
            time_spent: {
              expression: (c: any) => /^([0-9]?[0-9]?[0-9]):[0-9]?[0-9]?[0-9]$/.test(c.value),
              message: (error: any, field: FormlyFieldConfig) => `"${field.formControl?.value}" is not valid`
            }
          }
  
        },
        {
          className: 'col-6',
          type: 'select',
          key: 'status',
          templateOptions: {
            label: 'Status',
            required: true,
            options: [
              {label:'To do',value:'To do'},
              // {label:'Blocked',value:'Blocked' },
              // {label:'In_progress',value:'In_progress' },
              {label:'Done',value:'Done' }
  
            ],
          },
  
        },
      ],
    },
  
  ];
}

// export let Forms = [
//   {
//     key: 'task',
//     type: 'input',
//     templateOptions: {
//       placeholder: 'Name',
//       label: 'Task Name',
//       required: true
//     }
//   },
//   {
//     type: 'textarea',
//     key: 'taskDetails',
//     templateOptions: {
//       label: 'Task Description',
//       placeholder: 'Description',
//       rows: 6,
//     },
//   },
//   {
//     fieldGroupClassName: 'row',
//     fieldGroup: [
//       {
//         className: 'col-6',
//         key: 'project',
//         type: 'select',
//         templateOptions: {
//           placeholder: 'Select Project',
//           label: 'Project',
//           required: true,
//           options: [
//             { label: 'ebs_live', value: 'ebs_live', },
//           ],
//         },
//         required: true
//       },
//       {
//         className: 'col-6',
//         key: 'assignee',
//         type: 'select',
//         templateOptions: {
//           placeholder: 'Select Assignee',
//           label: 'Assignee',
//           required: true,
//           options: [
//             { label: 'ebs_live', value: 'ebs_live', },
//           ],
//         },
//         required: true
//       },
//     ]
//   },
  
//   {
//     fieldGroupClassName: 'row',
//     fieldGroup: [
//       {
//         className: 'col-6',
//         key: 'startDate',
//         type: 'datePickr',
//         defaultValue: moment().toDate(),
//         templateOptions: {
//           placeholder: 'DD/MM/YY',
//           label: 'Start date',
//           dateFormat:'DD/MM/YY',
//           required: true,
//         },
//       },
//       {
//         className: 'col-6',
//         type: 'datePickr',
//         key: 'endDate',
//         required: true,
//         defaultValue: moment().toDate(),
//         templateOptions: {
//           label: 'End date',
//           placeholder: 'DD/MM/YY',
//           dateFormat:'DD/MM/YY',
//           required: true,
//         },

//       },
//     ],
//   },
//   {
//     fieldGroupClassName: 'row',
//     fieldGroup: [
//       {
//         className: 'col-6',
//         type: 'input',
//         key: 'timeSpent',
//         required: true,
//         templateOptions: {
//           type: 'text',
//           placeholder: '00:00',
//           required: true,
//           label: 'Time spent',
//         },
//         validators: {
//           time_spent: {
//             expression: (c: any) => /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/.test(c.value),
//             message: (error: any, field: FormlyFieldConfig) => `"${field.formControl?.value}" is not valid`
//           }
//         }

//       },
//       {
//         className: 'col-6',
//         type: 'select',
//         key: 'status',
//         templateOptions: {
//           label: 'Status',
//           required: true,
//           options: [
//             { label: 'done', value: 'Done' },

//           ],
//         },

//       },
//     ],
//   },

// ];