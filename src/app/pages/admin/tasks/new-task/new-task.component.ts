import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../../services/api.service';
import { formlyForm } from './form';
import { forkJoin, Observable } from 'rxjs';
import * as moment from 'moment';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.scss'],
})
export class NewTaskComponent implements OnInit {
  title: string = 'Create a new task';
  herderPreTitle: string = 'New Task';
  typeOfFrom = 'Create';
  data_id: String;

  form = new FormGroup({});
  model = {};
  fields: any;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
// Below function is used to gether all the required data for form 
// we are using project and customer api to merge there detail in this form
// After success we move the data to formly file which make them in dropdown.
// Also we check if route is of update then we show update form were all field are pre filled.
  ngOnInit(): void {
    this.getFormData().subscribe((response: any) => {
      let assigneeOptions: any = [];
      let projectOptions: any = [];
    
      response[0].forEach((assignee: any) => {
        assigneeOptions.push({
          label: assignee.firstName,
          value: assignee._id,
        });
      });
      
      response[1].data.forEach((project: any) => {
        projectOptions.push({
          label: project.projectName,
          value: project._id,
        });
      });

      this.fields = formlyForm({
        assignee: assigneeOptions,
        project: projectOptions,
      });
    });
    this.route.data.subscribe((meta) => {
      console.log(meta);
      if (meta.type == 'update') {
        this.route.params.subscribe((params) => {
          console.log('params', params);
          if (params['id']) {
            this.title = 'Update Task';
            this.herderPreTitle = 'Update task';
            this.typeOfFrom = 'Update';
            this.data_id = params['id'];
            this.apiService.get('tasks/' + params['id']).subscribe(
              (response: any) => {
                // console.log('response', response);
                let data = response.data;
                data.startDate = moment(data.startDate).toDate();
                data.endDate = moment(data.endDate).toDate();
                this.model = data;
              },
              (error) => {
                // console.log(error)
              }
            );
          } else {
            this.router.navigate(['admin/customers/listing']);
          }
        });
      }
    });
  }
  //Below function is used to submit the data for saveing in db.
  submit(data: any) {
    if (this.form.invalid) return;
    if (this.data_id) {
      data._id = this.data_id;
    }
    this.apiService.post('tasks/new_task', data).subscribe(
      (response) => {
        this.apiService.toaster('success', {
          code: '',
          message: 'Task Added successfully!!',
        });
        this.form.reset();
        this.router.navigate(['admin/tasks/listing']);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getFormData(): Observable<any> {
    const assignee = this.apiService.get('users/all');
    const project = this.apiService.get('projects/all');
    return forkJoin([assignee, project]);
  }
  cancelNewtaskCreation() {
    this.router.navigate(['admin/tasks/listing']);
  }
}
