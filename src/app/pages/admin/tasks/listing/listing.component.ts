import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Tasks } from '../tasks.model';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.scss'],
})
export class ListingComponent implements OnInit {
  title = 'Tasks';
  module = 'tasks';
  headerPreTitle = 'Tasks';
  tasks: Tasks[] = [];
  duplicatedData = {
    key: 'status',
  };
  tableOption = {
    duplicate: true,
  };
  dataTable = {
    totalRecord: 0,
    perPage: 10,
    skip:0,
    search:null,
    headers: [
      { label: 'Task Name', key: 'taskName', order:0 },
      { label: 'Project', key: 'project' , order:0},
      { label: 'Assignee', key: 'assignee' , order: 0},
      { label: 'Time Spent', key: 'timeSpent' , order:0},
      { label: 'Start Date', key: 'startDate' , order:0},
      { label: 'End date', key: 'endDate' ,order:0},
      { label: 'Status', key: 'status', order : 0 },
    ],
    data: [{}],
    filter: [{name: 'status', data: [{name: 'Done'}, {name: 'To do'}, {name: 'Any'}], result: 'Any', label: 'Status'}]
  };

  constructor(private apiService: ApiService) {}
  // Following function is called very first time. And this function is used to retrieved the data 
  // from db and is used to list them in table.
  // the same function call again and again when user sort, filter or search the data
  ngOnInit(): void {
  let limit = this.dataTable.perPage;
  let skip = this.dataTable.skip;
  let search = this.dataTable.search;
  let sort = this.dataTable.headers.filter(ele => ele.order != 0)
  let filters = this.dataTable.filter.map((ele:any)=>{ return {  [ele.name]: ele.result}}).filter((ele:any)=> ele.status != 'Any')
  let queryParams = '?limit=' + limit+'&&skip='+skip+'&&search='+(search ? search : 'null') + '&&sort_column='+(sort.length > 0 ? sort[0].key : 'null') + '&&sort=' + (sort.length > 0 ? sort[0].order==1? 'asc' : 'desc' : 'null') + '&&filter='+ (filters.length > 0 ?JSON.stringify(filters) : null)
    this.apiService.get('tasks/all' + queryParams).subscribe(
      (response: any) => {
        this.dataTable.totalRecord = response.totalRecord;
        this.dataTable.perPage = limit;
        this.tasks = response.data.map((task: any) => {
         

          if (task.project !== null) {
            task.project = task.project.projectName;
          } else {
            task.project = 'Not assigned';
            task.status = 'Not started';
          }
          task.startDate = Array.from(task.startDate.split('T'))[0];
          task.endDate = Array.from(task.endDate.split('T'))[0];
          return task;
        });
        this.dataTable.data = this.tasks;
      },
      (error : any) => {}
    );
  }

  //This function is used to duplicate the task row , while duplicating we use the data of the row and submit it 
  // without _id and it creates the new row.
  getDuplicate(event: any) {
    this.apiService.get('tasks/' + event).subscribe(
      (response: any) => {
        //  console.log(response)
        let dataToCopy = response.data;
        delete dataToCopy._id;
        this.apiService.post('tasks/new_task', dataToCopy).subscribe(
          (response) => {
            this.apiService.toaster('success', {
              code: '',
              message: 'Duplicate task added successfully!!',
            });
            this.ngOnInit();
          },
          (error) => {
            console.log(error);
          }
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }
  configChange(config:any){
    this.dataTable = config
    this.ngOnInit();
  }
  
}
