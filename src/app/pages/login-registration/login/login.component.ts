import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Forms } from './forms';
// import {ApiService} from '../../../services/api.service'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form = new FormGroup({});
  model = {};
  fields = Forms

  constructor(private loginService: ApiService,private router:Router)
  {
  // Here we are checking if user is already loged in we move him to another route
     if(this.loginService.loggedIn()){
       this.router.navigate(['live']);
     }
   }

  ngOnInit(): void {
    
  }
  // This function is used to submit the login form and hit the api to check if cred are corret or Not
  // If it is correct we save the token in localStorage.
  onSubmit(data) {
    if(!this.form.valid) return; 
    this.loginService.post('auth/login', data).subscribe((response: any) => {
     this.router.navigate(['live']);  
      localStorage.setItem('token',response['access_token']);
    },
    (error)=>{
    }
    )
  }

}
