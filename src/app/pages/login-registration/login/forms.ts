import { FormlyFieldConfig } from '@ngx-formly/core'

export let Forms = [
    {
      key: 'email',
      type: 'input',
      templateOptions: {
        placeholder: 'Email',
        required: true,
      }
    },
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        placeholder: 'Password',
        required: true,
      }
    }
  ];