import { FormlyFieldConfig } from '@ngx-formly/core'

export let Forms = [
    {
        key: 'email',
        type: 'input',
        templateOptions: {
            type: 'email',
            placeholder: 'Email',
            required: true,
        },
        validators: {
            ip: {
                expression: (c) => {
                    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(c.value)
                },
                message: (error, field: FormlyFieldConfig) => `"${field.formControl?.value}" is not a valid Email Address`,
            },
        },
    }
]