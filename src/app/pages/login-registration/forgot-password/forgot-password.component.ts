import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Forms} from './forms'
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  form = new FormGroup({});
  model = {};
  fields = Forms
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(data){
    console.log('data',data);
  }
}
