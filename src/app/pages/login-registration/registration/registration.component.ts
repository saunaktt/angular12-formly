import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Forms } from './forms';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  form = new FormGroup({
    liveEvent: new FormControl(''),
    generalConditions: new FormControl('')
  });

  model = {};
  submitted = false;
  // acceptRadio={'liveEvent':false,'generalConditions':false};
  fields = Forms;
  liveEventChecked: boolean = false;
  conditionsChecked: boolean = false;
  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit(): void {
  }
  onSubmit(formDirective) {

    this.submitted = true;
    if (!this.form.valid) return;
    let backup = JSON.parse(JSON.stringify(this.model));
    this.apiService.post("auth/register", this.model).subscribe((response: any) => {
      this.submitted = false;
      window.scroll(0, 0);
      this.form.reset();
      formDirective.resetForm();
      this.apiService.post('auth/login', backup).subscribe((response: any) => {
        this.router.navigate(['live']);
        localStorage.setItem('token', response['access_token']);
      },
        (error) => {
        }
      )
      // this.apiService.toaster('success',{code:'',message:'successfully registered!!'});

    },
      (error) => {
        console.log(error)
        this.apiService.toaster('error', { code: '', message: 'some error occurred' });
      }
    );
  }
}
