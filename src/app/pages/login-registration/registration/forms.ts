import { FormlyFieldConfig } from '@ngx-formly/core'

export let Forms = [
    {
        validators:{
            validation:[
             {name: 'fieldMatch', options: { errorPath: 'passwordConfirm' }}
            ]
        },
        fieldGroup:[
         {
             key: 'firstName',
             type: 'input',
             templateOptions: {
                 placeholder: 'First Name',
                 required: true,
             }
         },
         {
             key: 'lastName',
             type: 'input',
             templateOptions: {
                 placeholder: 'Last Name',
                 required: true,
             }
         },
         {
             key: 'company',
             type: 'input',
             templateOptions: {
                 placeholder: 'Company',
                 required: true,
             }
         },
         {
             key: 'email',
             type: 'input',
             templateOptions: {
                 type: 'email',
                 placeholder: 'Email',
                 required: true,
             },
             validators: {
                 ip: {
                     expression: (c) => {
                         return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(c.value)
                     },
                     message: (error, field: FormlyFieldConfig) => `"${field.formControl?.value}" is not a valid Email Address`,
                 },
             },
         },
         {
             key: 'password',
             type: 'input',
             templateOptions: {
                 type: 'password',
                 placeholder: 'Password',
                 required: true,
             }
         },
         {
             key: 'passwordConfirm',
             type: 'input',
             templateOptions: {
                 type: 'password',
                 placeholder: 'Confirm Password',
                 required: true,
             }
         }
        ]
    }
 
 ]