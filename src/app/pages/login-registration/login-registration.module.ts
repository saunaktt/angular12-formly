import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { RouterModule } from "@angular/router";
import { LoginRegistrationComponent } from '../../shared_layouts/login-registration/login-registration.component';
import { NgxModule } from '../../shared_module/ngx.module';
import { FormlyFormModule } from '../../shared_module/formly-form.module';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { FormsModule } from '@angular/forms';

const ROUTE = [
  { path: 'login', component: LoginComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'forgot_password', component: ForgotPasswordComponent },
  { path: 'change_password', component: ChangePasswordComponent }
];


@NgModule({
  declarations: [
    LoginRegistrationComponent,
    LoginComponent,
    RegistrationComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    NgxModule,
    FormlyFormModule,
    FormsModule,
    RouterModule.forChild(ROUTE),
  ]
})
export class LoginRegistrationModule { }
