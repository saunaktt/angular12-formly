import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Forms} from './forms'
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  form = new FormGroup({});
  model = {};
  fields = Forms
  constructor() { }

  ngOnInit(): void {
  }
  onSubmit(data){
    console.log('data',data);
  }

}
