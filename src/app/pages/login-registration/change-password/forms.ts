import { FormlyFieldConfig } from '@ngx-formly/core'

export let Forms = [
    {
      key: 'password',
      type: 'input',
      templateOptions: {
        type: 'password',
        placeholder: 'Password',
        required: true,
      }
    },
    {
      key: 'confirmPassword',
      type: 'input',
      templateOptions: {
        type: 'password',
        placeholder: 'Confirm Password',
        required: true,
      }
    }
  ];